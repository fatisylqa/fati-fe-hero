import React from "react";
import logo from "./logo.svg";
import "./App.css";
import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
  HttpLink,
  ApolloProvider,
  gql,
} from "@apollo/client";

import { useLazyQuery } from "@apollo/react-hooks";

import Stations from "./Stations";

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    uri: "https://tapi-iota.vercel.app/api/gql",
  }),
});

function App() {
  return (
    <ApolloProvider client={client}>
      <h1>Terap trial task</h1>
      <Stations />
    </ApolloProvider>
  );
}

export default App;
