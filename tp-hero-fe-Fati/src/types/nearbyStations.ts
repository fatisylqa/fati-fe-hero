/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: nearbyStations
// ====================================================

export interface nearbyStations_nearby_stations {
  __typename: "Station";
  name: string;
}

export interface nearbyStations_nearby {
  __typename: "Nearby";
  stations: nearbyStations_nearby_stations[];
}

export interface nearbyStations {
  nearby: nearbyStations_nearby;
}


