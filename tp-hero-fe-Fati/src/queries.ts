import gql from "graphql-tag";

export const GET_STATIONS = gql`
  query nearbyStations {
    nearby(latitude: 48.1390056, longitude: 11.5577837, radius: 2000) {
      stations(count: 5) {
        name
      }
    }
  }
`;
