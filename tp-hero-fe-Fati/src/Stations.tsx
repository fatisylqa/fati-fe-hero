import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_STATIONS } from "./queries";
import {
  nearbyStations,
  nearbyStations_nearby,
  nearbyStations_nearby_stations,
} from "./types/nearbyStations";

import MUIDataTable from "mui-datatables";
import CircularProgress from "@material-ui/core/CircularProgress";


const Stations: React.FC = () => {
  const { loading, data, error } = useQuery<nearbyStations>(GET_STATIONS);

  if (loading) return <CircularProgress />;
  if (error) return <p>error</p>;
  console.log("test data", data?.nearby.stations);

  const columns = [
    {
      name: "name",
      label: "Name of stations near Munich",
      options: {
        filter: true,
        sort: true,
      },
    },
  ];
  
  const options = {
    filterType: "checkbox",
  };

  let stationNames = data?.nearby.stations.map((station) => station);



  return (
    <MUIDataTable
      title={"Train station list"}
      data={stationNames}
      columns={columns}
      options={options}
    />
  );
};

export default Stations;
